<?php
namespace Drupal\questionnaire_question\Types;

/**
 * A class for defining a condition exportable.
 */
abstract class QuestionType implements QuestionTypeInterface {
  // Every question has an answer datatype and form element property.
  protected $answer_type;
  protected $form_element;

  /**
   * Class constructor.
   *
   *
   * @return
   *   An instance of this plugin.
   */
  public function __construct() {
  }

  /**
   * Creates the form element for this question type.
   *
   * @return $form_element.
   *   The form element for this question type.
   */
  public function getcontrol($nid, $answer) {
  }

  /**
   * Adds a value for either the answernumber or answertext property.
   * And performs any other necessary manipulations on the answer.
   *
   * @return $answer.
   *   The altered answer object for this question type.
   */
  public function handleanswer($answer, $answers) {
    $answer->answertext = $answer->answer;
    $answer->answernumber = NULL;
  }
}
