<?php
/**
 * @file
 * QuestionType: Message.
 *
 */

namespace Drupal\questionnaire_question\Plugins\QuestionType;
use Drupal\questionnaire_question\Types\QuestionType;

$plugin = array(
  'class' => '\\Drupal\\questionnaire_question\\Plugins\\QuestionType\\Message',
  'defaults' => array('answer_type' => ''),
);

/**
 * Class Message.
 *
 * @package Drupal\questionnaire_question\QuestionType
 */
class Message extends QuestionType {
  
  /**
   * Class constructor override.
   */
  public function __construct() {
    $this->answer_type = '';
    $this->form_element = array();
  }

  /**
   * Creates the form element for this question type.
   *
   * @param $nid
   *   The node id of the Question this plugin is attached to.
   * @param $answer
   *   The answer to the Question this plugin is attached to.
   *
   * @return $form_element.
   *   The form element for this question type.
   */
  public function getcontrol($nid, $answer = NULL) {
    $node_object = node_load($nid);
    $node = entity_metadata_wrapper('node', $node_object);

    if (isset($node) && $node != FALSE){
      // This is really a quite different situation.
      $this->form_element = array(
        '#markup' => '<p>' . check_plain($node->title->value()) . '</p>'
        );
      return $this->form_element;
    }
  }
}