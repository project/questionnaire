<?php
/**
 * @file
 * QuestionType: Checkboxes.
 *
 */

namespace Drupal\questionnaire_question\Plugins\QuestionType;
use Drupal\questionnaire_question\Types\QuestionType;

$plugin = array(
  'class' => '\\Drupal\\questionnaire_question\\Plugins\\QuestionType\\Checkboxes',
  'defaults' => array('answer_type' => 'Array'),
);

/**
 * Class Checkboxes.
 *
 * @package Drupal\questionnaire_question\QuestionType
 */
class Checkboxes extends QuestionType {
  protected $options;
  
  /**
   * Class constructor override.
   */
  public function __construct() {
    $this->answer_type = 'Array';
    $this->form_element = array();
    $this->options = NULL;
  }

  /**
   * Creates the form element for this question type.
   *
   * @param $nid
   *   The node id of the Question this plugin is attached to.
   * @param $answer
   *   The answer to the Question this plugin is attached to.
   *
   * @return $form_element.
   *   The form element for this question type.
   */
  public function getcontrol($nid, $answer) {
    $node_object = node_load($nid);
    $node = entity_metadata_wrapper('node', $node_object);

    if (isset($node) && $node != FALSE){
      $formtype = 'checkboxes';
        $description = t('Please select the appropriate options');
        $options = questionnaire_question_allowed_values($node_object->questionnaire_question_options[$node->language->value()]);
        if ($answer == '') {
          $answer = array();
        }
      $this->form_element = array(
          '#type' => $formtype,
          '#title' => $node->nid->value() . ' ' . check_plain($node->title->value()),
          '#description' => $description,
          '#default_value' => $answer,
      );
      if (isset($options)) {
        $this->form_element['#options'] = $options;
      }
      return $this->form_element;
    }
  }
}