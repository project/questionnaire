<?php
/**
 * @file
 * QuestionType: Time.
 *
 */

namespace Drupal\questionnaire_question\Plugins\QuestionType;
use Drupal\questionnaire_question\Types\QuestionType;

$plugin = array(
  'class' => '\\Drupal\\questionnaire_question\\Plugins\\QuestionType\\Time',
  'defaults' => array('answer_type' => 'String'),
);

/**
 * Class Time.
 *
 * @package Drupal\questionnaire_question\QuestionType
 */
class Time extends QuestionType {
  
  /**
   * Class constructor override.
   */
  public function __construct() {
    $this->answer_type = 'String';
    $this->form_element = array();
  }

  /**
   * Creates the form element for this question type.
   *
   * @param $nid
   *   The node id of the Question this plugin is attached to.
   * @param $answer
   *   The answer to the Question this plugin is attached to.
   *
   * @return $form_element.
   *   The form element for this question type.
   */
  public function getcontrol($nid, $answer) {
    $node_object = node_load($nid);
    $node = entity_metadata_wrapper('node', $node_object);

    if (isset($node) && $node != FALSE){
      $formtype = 'textfield';
      $description = t('Please provide a time in the format HH:MM');
      $this->form_element = array(
          '#type' => $formtype,
          '#title' => $node->nid->value() . ' ' . check_plain($node->title->value()),
          '#description' => $description,
          '#default_value' => $answer,
      );
      return $this->form_element;
    }
  }
}