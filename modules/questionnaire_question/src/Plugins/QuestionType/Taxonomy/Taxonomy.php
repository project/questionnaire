<?php
/**
 * @file
 * QuestionType: Taxonomy.
 *
 */

namespace Drupal\questionnaire_question\Plugins\QuestionType;
use Drupal\questionnaire_question\Types\QuestionType;

$plugin = array(
  'class' => '\\Drupal\\questionnaire_question\\Plugins\\QuestionType\\Taxonomy',
  'defaults' => array('answer_type' => 'String'),
);

/**
 * Class Taxonomy.
 *
 * @package Drupal\questionnaire_question\QuestionType
 */
class Taxonomy extends QuestionType {
  
  /**
   * Class constructor override.
   */
  public function __construct() {
    $this->answer_type = 'String';
    $this->form_element = array();
  }

  /**
   * Creates the form element for this question type.
   *
   * @param $nid
   *   The node id of the Question this plugin is attached to.
   * @param $answer
   *   The answer to the Question this plugin is attached to.
   *
   * @return $form_element.
   *   The form element for this question type.
   */
  public function getcontrol($nid, $answer) {
    $node_object = node_load($nid);
    $node = entity_metadata_wrapper('node', $node_object);

    if (isset($node) && $node != FALSE){
      if (isset($node->qq_options_vocabulary)) {
       $vocabulary = taxonomy_vocabulary_machine_name_load($node->qq_options_vocabulary->value());
      }
      $formtype = 'textfield';
      $description = t('Please provide the appropriate term');
      // For the taxonomy type questions, the textfield calls the
      // taxonomy_autocomplete() function.
      // see https://www.drupal.org/node/854216
      $autocomplete_path = 'taxonomy/autocomplete/field_' . $vocabulary->machine_name;
      $this->form_element = array(
          '#type' => $formtype,
          '#title' => $node->nid->value() . ' ' . check_plain($node->title->value()),
          '#description' => $description,
          '#default_value' => $answer,
      );
      if (isset($autocomplete_path)) {
        $control['#autocomplete_path'] = $autocomplete_path;
      }
      return $this->form_element;
    }
  }

  /**
   * Override.
   * Additionally attach the answer to the answerobject as a taxonomy term. 
   *
   * @return $answer.
   *   The altered answer object for this question type.
   */
  public function handleanswer($answer, $answers) {
    // Load the vocabulary using the question and use it to get the correct
    // taxonomy term.
    $question_node = node_load($answer->question);
    $question = entity_metadata_wrapper('node', $question_node);
    if (isset($node->qq_options_vocabulary)) {
      $vocabulary_machinename = $question->qq_options_vocabulary->value();
      $term = taxonomy_get_term_by_name($answer->answer, $vocabulary_machinename);
      // Only use the first term that matches answer.
      $term = reset($term);
      if (empty($term)) {
        // If the questionnaire_fuzzy module is enabled, save the answer as
        // fuzzy.
        if (module_exists('questionnaire_fuzzy')) {
          $answer->fuzzy = $answer->answer;
          $answer->answer = '';
          $answer->answernumber = NULL;
          return;
        }
        // Otherwise create a new term.
        else {
          $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machinename);
          $newterm = new stdClass();
          $newterm->name = $answer->answer;
          $newterm->vid = $vocabulary->vid;
          $newterm->parent = 0;
          taxonomy_term_save($newterm);
          // Load the created term.
          $term = taxonomy_get_term_by_name($answer->answer, $vocabulary_machinename);
          $term = reset($term);
        }
      }

      $answer->answertext = $answer->answer;

      // Add the answer to the $terms array for the answer object.
      $vocabulary_fieldname = 'field_' . $vocabulary_machinename;
      $terms[$vocabulary_fieldname] = $term->tid;
    }
  }
}