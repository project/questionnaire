<?php
/**
 * @file
 * QuestionType: Open.
 *
 */

namespace Drupal\questionnaire_question\Plugins\QuestionType;
use Drupal\questionnaire_question\Types\QuestionType;

$plugin = array(
  'class' => '\\Drupal\\questionnaire_question\\Plugins\\QuestionType\\Open',
  'defaults' => array('answer_type' => 'String'),
);

/**
 * Class Open.
 *
 * @package Drupal\questionnaire_question\QuestionType
 */
class Open extends QuestionType {
  
  /**
   * Class constructor override.
   */
  public function __construct() {
    $this->answer_type = 'String';
    $this->form_element = array();
  }

  /**
   * Creates the form element for this question type.
   *
   * @param $nid
   *   The node id of the Question this plugin is attached to.
   * @param $answer
   *   The answer to the Question this plugin is attached to.
   *
   * @return $form_element.
   *   The form element for this question type.
   */
  public function getcontrol($nid, $answer) {
    $node_object = node_load($nid);
    $node = entity_metadata_wrapper('node', $node_object);

    if (isset($node) && $node != FALSE){
      $formtype = 'textfield';
      $description = t('Please provide free text.');
      $this->form_element = array(
          '#type' => $formtype,
          '#title' => $node->nid->value() . ' ' . check_plain($node->title->value()),
          '#description' => $description,
          '#default_value' => $answer,
      );
      return $this->form_element;
    }
  }
 
  /**
   * Override.
   * If the questionnaire_fuzzy module is enabled, save the answer as
   * fuzzy.
   * Otherwise save the answer as text.
   *
   * @return $answer.
   *   The altered answer object for this question type.
   */
  public function handleanswer($answer, $answers) {
    if (module_exists('questionnaire_fuzzy')) {
      $answer->fuzzy = $answer->answer;
      $answer->answer = '';
      $answer->answernumber = NULL;
    }
    else {
      $answer->answertext = $answer->answer;
      $answer->answernumber = NULL;
    }
  }
}