
(function($) {

/**
 * @ingroup admin_behaviors
 * @{
 */

/**
 * Click a button to test the services
 *
 */
Drupal.behaviors.questionnairetestServices = {
	attach: function (context, settings) { 
      $("#questionnaire-testservices", context).once("init-questionnaire-testServices",  function() {
	    $(this).click( function() {

	     
         var node = {
	  	     'apiPath': Drupal.settings.basePath + 'api/node'
 	      };
	      // REST functions.
	      node.submit = function(node) {
	        $.ajax({
	          type: "POST",
	          url: this.apiPath,
	          data: JSON.stringify(node),
	          dataType: 'json',
	          contentType: 'application/json',
	          success: function(result) {
	            alert("result" + result);
	          },
	          error: function (err) {
	          	alert (err.statusText); // This gives the proper Drupal Error (for example if you store a string in a number question).
	          }
	       });
	     };
	     mynode = {
	    		 title : "reinier",
	    		 type : "page",
	    		 body : {und: {}},
	    		 field_me : {und : {}},
	    		 language : "und"
	     };
	     mynode['body']['und'][0] = {};
	     mynode['body']['und'][0]['value'] = "here we are!";
	     mynode['field_me']['und'][0] = {};
	     mynode['field_me']['und'][0] = 11;
	     node.submit(mynode);	    	
	    	
	    });
      });
	}
};

Drupal.behaviors.addOperandVocabulary = {
        attach: function (context, settings) {
            $('input#edit-questionnaire-question-add-condition').click(function () {
                var operand = $('select#edit-questionnaire-question-vocabulary-options').val();
                var operator = $('select#edit-questionnaire-question-operator-options').val();
                var $t = document.getElementById('edit-questionnaire-question-vocabulary');
                var old_condition = $t.value;
                $t.value = '';
                // if the vocabulary textbox is empty, just insert
                // (operand1 OP operand2 ... operandN)
                var condition = "(" + operand[0];
                for (i=1; i<operand.length; i++) {
                    condition += (" " + operator + " " + operand[i]);
                }
                if (old_condition != '' && old_condition != '()') {
                    condition += (" " + operator + " " + old_condition);
                }
                // else if there is something already in the vocabulary textbox
                // insert
                // (operand1 OP (what was present))
                condition += (")");
                $t.insertAtCaret(condition);
                return false;
            });
            HTMLTextAreaElement.prototype.insertAtCaret = function (text) {
              text = text || '';
              if (document.selection) {
                // IE
                this.focus();
                var sel = document.selection.createRange();
                sel.text = text;
              } else if (this.selectionStart || this.selectionStart === 0) {
                // Others
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                this.value = this.value.substring(0, startPos) +
                  text +
                  this.value.substring(endPos, this.value.length);
                this.selectionStart = startPos + text.length;
                this.selectionEnd = startPos + text.length;
              } else {
                this.value += text;
              }
            };
        }

    };

Drupal.behaviors.addFromQuestionnaire = {
        attach: function (context, settings) {
            $('input#edit-questionnaire-question-from-questionnaire').click(function () {
                var fromquestionnaire = '{$questionnaire}.';
                var $t = document.getElementById('edit-questionnaire-question-vocabulary');
                $t.insertAtCaret(fromquestionnaire);
                return false;
            });
            HTMLTextAreaElement.prototype.insertAtCaret = function (text) {
              text = text || '';
              if (document.selection) {
                // IE
                this.focus();
                var sel = document.selection.createRange();
                sel.text = text;
              } else if (this.selectionStart || this.selectionStart === 0) {
                // Others
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                this.value = this.value.substring(0, startPos) +
                  text +
                  this.value.substring(endPos, this.value.length);
                this.selectionStart = startPos + text.length;
                this.selectionEnd = startPos + text.length;
              } else {
                this.value += text;
              }
            };
        }

    };

Drupal.behaviors.addFromContext = {
        attach: function (context, settings) {
            $('input#edit-questionnaire-question-from-context').click(function () {
                var fromcontext = '{$context}.';
                var $t = document.getElementById('edit-questionnaire-question-vocabulary');
                $t.insertAtCaret(fromcontext);
                return false;
            });
            HTMLTextAreaElement.prototype.insertAtCaret = function (text) {
              text = text || '';
              if (document.selection) {
                // IE
                this.focus();
                var sel = document.selection.createRange();
                sel.text = text;
              } else if (this.selectionStart || this.selectionStart === 0) {
                // Others
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                this.value = this.value.substring(0, startPos) +
                  text +
                  this.value.substring(endPos, this.value.length);
                this.selectionStart = startPos + text.length;
                this.selectionEnd = startPos + text.length;
              } else {
                this.value += text;
              }
            };
        }

    };


Drupal.behaviors.clear_condition = {
  attach: function(context, settings) {
    $('input#edit-questionnaire-question-clear-condition').click(function () {
      document.getElementById('edit-questionnaire-question-vocabulary').value = '';
      return false;
    });
  }
}

/**
 * @} End of "defgroup admin_behaviors".
 */

})(jQuery);
