<?php

/**
 * @file
 * FeedsQuestionnaireSubmissionProcessor class.
 */

/**
 * Feeds processor plugin. Create users from feed items.
 */
class FeedsQuestionnaireSubmissionProcessor extends FeedsProcessor {
  /**
   * Define entity type.
   */
  public function entityType() {
    return 'questionnaire_submission';
  }

  /**
   * Returns the bundle.
   *
   * @return string
   *   The bundle of the entities this processor will create.
   */
  public function bundle() {
    $bundle = 'questionnaire_submission';
    return $bundle;
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('Questionnaire Submissions');
    return $info;
  }

  /**
   * Creates a new Questionnaire Submission in memory and returns it.
   */
  protected function newEntity(FeedsSource $source, $values = array()) {
    $submission = entity_get_controller('questionnaire_submission')->create($values);
    return $submission;
  }

  /**
   * Loads an existing Questionnaire Submission.
   */
  protected function entityLoad(FeedsSource $source, $sid) {
    $submission = parent::entityLoad($source, $nid);

    if ($this->config['update_existing'] != FEEDS_UPDATE_EXISTING) {
      $submission->uid = $this->config['author'];
    }

    // Populate properties that are set by node_object_prepare().
    if ($this->config['update_existing'] == FEEDS_UPDATE_EXISTING) {
      $submission->log = 'Updated by FeedsQuestionnaireSubmissionProcessor';
    }
    else {
      $submission->log = 'Replaced by FeedsQuestionnaireSubmissionProcessor';
    }
    return $submission;
  }

  /**
   * Validates a user account.
   */
  protected function entityValidate($submission) {
    if (empty($submission->questionnaire_id)) {
      throw new FeedsValidationException(t('Questionnaire Submission questionnaire ID missing.'));
    }
    if (empty($submission->context_id)) {
      throw new FeedsValidationException(t('Questionnaire Submission context ID missing.'));
    }
    if (empty($submission->context_type)) {
      throw new FeedsValidationException(t('Questionnaire Submission context type missing.'));
    }
    if (empty($submission->recipient_type)) {
      throw new FeedsValidationException(t('Questionnaire Submission recipient type missing.'));
    }
  }

  /**
   * Save a user account.
   */
  protected function entitySave($submission) {
    entity_save('questionnaire_submission', $submission);
  }

  /**
   * Delete multiple user accounts.
   */
  protected function entityDeleteMultiple($sids) {
    entity_delete_multiple('questionnaire_submission', $sids);
  }

  /**
   * Override setTargetElement to operate on a target item that is a
   * Questionnaire Submission.
   */
  public function setTargetElement(FeedsSource $source, $target_submission, $target_element, $value) {
    switch ($target_element) {
      case 'created':
        $target_submission->created = feeds_to_unixtime($value, REQUEST_TIME);
        break;
      case 'changed':
        $target_submission->changed = feeds_to_unixtime($value, REQUEST_TIME);
        break;
      case 'recipient':
        // Get the entity using it's feeds guid, store its id in the database.
        $recipient = db_select('feeds_item', 'fi')
            ->fields('fi', array('entity_id'))
            ->condition('guid', $value,'=')
            ->execute()
            ->fetchField();
        if (empty($recipient)) {
          $recipient = db_select('users', 'u')
            ->fields('u', array('uid'))
            ->condition('uuid', $value,'=')
            ->execute()
            ->fetchField();
        }
        if (!empty($recipient)) {
          $target_submission->recipient_id = $recipient;
        }
        break;
      case 'context_id':
      case 'questionnaire_id':
        // Get the entity using it's feeds guid, store its id in the database.
        $node = db_select('feeds_item', 'fi')
            ->fields('fi', array('entity_id'))
            ->condition('guid', $value,'=')
            ->execute()
            ->fetchField();
        if (empty($node)) {
          $node = db_select('node', 'n')
            ->fields('n', array('nid'))
            ->condition('uuid', $value,'=')
            ->execute()
            ->fetchField();
        }
        if (!empty($node)) {
          $target_submission->$target_element= $node;
        }
        break;
      default:
        parent::setTargetElement($source, $target_submission, $target_element, $value);
        break;
    }
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();

    $info = array('bundle' => $this->bundle());
    $wrapper = entity_metadata_wrapper($this->entityType(), NULL, $info);
    foreach ($wrapper->getPropertyInfo() as $name => $property) {
      if (!empty($property['setter callback']) && empty($property['field'])) {
        $guid_properties = array('recipient', 'context_id', 'questionnaire_id');
        $targets[$name] = array(
          'name' => (in_array($name, $guid_properties))? check_plain($property['label'] . t(' (Fill in value using Feeds GUID)')) : check_plain($property['label']),
          'description' => isset($property['description']) ? $property['description'] : NULL,
        );
      }
    }

    $entity_info = $this->entityInfo();
    $targets[$entity_info['entity keys']['id']]['optional_unique'] = TRUE;

    // Remove the bundle target.
    if (isset($entity_info['bundle keys']['bundle'])) {
      unset($targets[$entity_info['bundle keys']['bundle']]);
    }

    // Let other modules expose mapping targets.
    self::loadMappers();
    $entity_type = $this->entityType();
    $bundle = $this->bundle();
    drupal_alter('feeds_processor_targets', $targets, $entity_type, $bundle);

    return $targets;
  }
}
