<?php

/**
 * @file
 * FeedsQuestionnaireAnswerProcessor class.
 */

/**
 * Feeds processor plugin. Create users from feed items.
 */
class FeedsQuestionnaireAnswerProcessor extends FeedsProcessor {
  /**
   * Define entity type.
   */
  public function entityType() {
    return 'questionnaire_answer';
  }

  /**
   * Returns the bundle.
   *
   * @return string
   *   The bundle of the entities this processor will create.
   */
  public function bundle() {
    $bundle = 'questionnaire_answer';
    return $bundle;
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('Questionnaire Answers');
    return $info;
  }

  /**
   * Creates a new Questionnaire Answer in memory and returns it.
   */
  protected function newEntity(FeedsSource $source, $values = array()) {
    $answer = entity_get_controller('questionnaire_answer')->create($values);
    return $answer;
  }

  /**
   * Loads an existing Questionnaire Answer.
   */
  protected function entityLoad(FeedsSource $source, $sid) {
    $answer = parent::entityLoad($source, $nid);

    if ($this->config['update_existing'] != FEEDS_UPDATE_EXISTING) {
      $answer->uid = $this->config['author'];
    }

    // Populate properties that are set by node_object_prepare().
    if ($this->config['update_existing'] == FEEDS_UPDATE_EXISTING) {
      $answer->log = 'Updated by FeedsQuestionnaireAnswerProcessor';
    }
    else {
      $answer->log = 'Replaced by FeedsQuestionnaireAnswerProcessor';
    }
    return $answer;
  }

  /**
   * Validates a user account.
   */
  protected function entityValidate($answer) {
    if (empty($answer->questionnaire_id)) {
      throw new FeedsValidationException(t('Questionnaire Answer questionnaire ID missing.'));
    }
    if (empty($answer->context_id)) {
      throw new FeedsValidationException(t('Questionnaire Answer context ID missing.'));
    }
    if (empty($answer->context_type)) {
      throw new FeedsValidationException(t('Questionnaire Answer context type missing.'));
    }
    if (empty($answer->recipient_type)) {
      throw new FeedsValidationException(t('Questionnaire Answer recipient type missing.'));
    }
    if (empty($answer->question)) {
      throw new FeedsValidationException(t('Questionnaire Answer question ID missing.'));
    }
    if (empty($answer->sid)) {
      throw new FeedsValidationException(t('Questionnaire Answer submission ID missing.'));
    }
  }

  /**
   * Save a questionnaire_answer.
   */
  protected function entitySave($answer) {
    $answer->terms = $this->attachtaxonomyterms($answer);
    if (isset($answer->terms)) {
      // expected format for now is 'taxonomy_field_name' => tid
      foreach ($answer->terms as $field => $term) {
        $answer->{$field}['und'][]['tid'] = $term;
      }
    }
    entity_save('questionnaire_answer', $answer);
  }

  /**
   * Delete multiple user accounts.
   */
  protected function entityDeleteMultiple($sids) {
    entity_delete_multiple('questionnaire_answer', $sids);
  }

  /**
   * Override setTargetElement to operate on a target item that is a
   * Questionnaire Answer.
   */
  public function setTargetElement(FeedsSource $source, $target_answer, $target_element, $value) {
    switch ($target_element) {
      case 'created':
        $target_answer->created = feeds_to_unixtime($value, REQUEST_TIME);
        break;
      case 'changed':
        $target_answer->changed = feeds_to_unixtime($value, REQUEST_TIME);
        break;
      case 'recipient':
        // Get the entity using it's feeds guid, store its id in the database.
        $recipient = db_select('feeds_item', 'fi')
            ->fields('fi', array('entity_id'))
            ->condition('guid', $value,'=')
            ->execute()
            ->fetchField();
        if (empty($recipient)) {
          $recipient = db_select('users', 'u')
            ->fields('u', array('uid'))
            ->condition('uuid', $value,'=')
            ->execute()
            ->fetchField();
        }
        if (!empty($recipient)) {
          $target_answer->recipient_id = $recipient;
        }
        break;
      case 'context_id':
      case 'questionnaire_id':
      case 'question':
        // Get the entity using it's feeds guid, store its id in the database.
        $nid = db_select('feeds_item', 'fi')
            ->fields('fi', array('entity_id'))
            ->condition('guid', $value,'=')
            ->execute()
            ->fetchField();
        if (empty($nid)) {
          $nid = db_select('node', 'n')
            ->fields('n', array('nid'))
            ->condition('uuid', $value,'=')
            ->execute()
            ->fetchField();
        }
        if (!empty($nid)) {
         $target_answer->$target_element = $nid;
        }
        break;
      case 'sid':
        // Get the entity using it's feeds guid, store its id in the database.
        $sid = db_select('feeds_item', 'fi')
            ->fields('fi', array('entity_id'))
            ->condition('guid', $value,'=')
            ->execute()
            ->fetchField();
        if (empty($sid)) {
          $sid = db_select('questionnaire_submission', 'qs')
            ->fields('qs', array('sid'))
            ->condition('uuid', $value,'=')
            ->execute()
            ->fetchField();
        }
        if (!empty($sid)) {
          $target_answer->sid = $sid;
        }
        break;
      default:
        parent::setTargetElement($source, $target_answer, $target_element, $value);
        break;
    }
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();

    $info = array('bundle' => $this->bundle());
    $wrapper = entity_metadata_wrapper($this->entityType(), NULL, $info);
    foreach ($wrapper->getPropertyInfo() as $name => $property) {
      if (!empty($property['setter callback']) && empty($property['field'])) {
        $guid_properties = array('recipient', 'context_id', 'questionnaire_id', 'question', 'sid');
        $targets[$name] = array(
          'name' => (in_array($name, $guid_properties))? check_plain($property['label'] . t(' (Fill in value using Feeds GUID)')) : check_plain($property['label']),
          'description' => isset($property['description']) ? $property['description'] : NULL,
        );
      }
    }

    $entity_info = $this->entityInfo();
    $targets[$entity_info['entity keys']['id']]['optional_unique'] = TRUE;

    // Remove the bundle target.
    if (isset($entity_info['bundle keys']['bundle'])) {
      unset($targets[$entity_info['bundle keys']['bundle']]);
    }

    // Let other modules expose mapping targets.
    self::loadMappers();
    $entity_type = $this->entityType();
    $bundle = $this->bundle();
    drupal_alter('feeds_processor_targets', $targets, $entity_type, $bundle);

    return $targets;
  }

  /**
   * Attach taxonomy terms.
   */
  protected function attachtaxonomyterms($answer) {
    // Attach taxonomy terms from context to answers.
    $questionnaire = node_load($answer->questionnaire_id);
    $submission = entity_load_single('questionnaire_submission', $answer->sid);
    $terms = array();
    if (variable_get('questionnaire_question_copy_termfields_from_context_' . $questionnaire->type, FALSE)) {
      $terms = $submission->addtaxonomyfields($submission->context_id);
      $context = node_load($submission->context_id);
      $fields = field_info_instances("node", $context->type);
      foreach ($fields as $field_name => $field) {
        $field_info = field_info_field($field_name);
        $type = $field_info['type'];
        if ($field_info['type'] == 'taxonomy_term_reference') {
          // TODO support more than 1 term per taxonomy
          // TODO do a strategy for multi-lingual (when there are more than 1 translation for a taxonomy term?)
          if(isset($context->{$field_name}[$context->language][0]['tid'])){
            $terms[$field_name] = $context->{$field_name}[$context->language][0]['tid'];
          }
        }
      }
    }
    return $terms;
  }
}
