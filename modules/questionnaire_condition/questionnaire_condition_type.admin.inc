<?php

/**
 * @file
 * QuestionnaireCondition type editing UI.
 */

/**
 * UI controller.
 */
class QuestionnaireConditionTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage questionnaire_condition entity types, including adding
		and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the questionnaire_condition type editing form.
 */
function questionnaire_condition_type_form($form, &$form_state, $questionnaire_condition_type, $op = 'edit') {

  if ($op == 'clone') {
    $questionnaire_condition_type->label .= ' (cloned)';
    $questionnaire_condition_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $questionnaire_condition_type->label,
    '#description' => t('The human-readable name of this questionnaire_condition type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($questionnaire_condition_type->type) ? $questionnaire_condition_type->type : '',
    '#maxlength' => 32,
//    '#disabled' => $questionnaire_condition_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'questionnaire_condition_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this questionnaire_condition type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['data']['#tree'] = TRUE;
  $form['data']['sample_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('An interesting questionnaire_condition switch'),
    '#default_value' => !empty($questionnaire_condition_type->data['sample_data']),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save questionnaire_condition type'),
    '#weight' => 40,
  );

  //Locking not supported yet
  /*if (!$questionnaire_condition_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete questionnaire_condition type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('questionnaire_condition_type_form_submit_delete')
    );
  }*/
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function questionnaire_condition_type_form_submit(&$form, &$form_state) {
  $questionnaire_condition_type = entity_ui_form_submit_build_entity($form, $form_state);
  $questionnaire_condition_type->save();
  $form_state['redirect'] = 'admin/structure/questionnaire_condition_types';
}

/**
 * Form API submit callback for the delete button.
 */
function questionnaire_condition_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/questionnaire_condition_types/manage/' . $form_state['questionnaire_condition_type']->type . '/delete';
}
