<?php

/**
 * @file
 * Example tpl file for theming a single questionnaire_condition-specific theme
 *
 * Available variables:
 * - $status: The variable to theme (while only show if you tick status)
 * 
 * Helper variables:
 * - $questionnaire_condition: The QuestionnaireCondition object this status is derived from
 */
?>

<div class="questionnaire_condition-status">
  <?php print '<strong>Questionnaire Condition Sample Data:</strong> ' . $questionnaire_condition_sample_data = ($questionnaire_condition_sample_data) ? 'Switch On' : 'Switch Off' ?>
</div>