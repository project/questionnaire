<?php

/**
 * @file
 * QuestionnaireCondition editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class QuestionnaireConditionUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'QuestionnaireConditions',
      'description' => 'Add edit and update questionnaire_conditions.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );
    
    // Change the overview menu type for the list of questionnaire_conditions.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    
    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a questionnaire_condition',
      'description' => 'Add a new questionnaire_condition',
      'page callback'  => 'questionnaire_condition_add_page',
      'access callback'  => 'questionnaire_condition_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'questionnaire_condition.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])

    );
    
    // Add menu items to add each different type of entity.
    foreach (questionnaire_condition_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label,
        'page callback' => 'questionnaire_condition_form_wrapper',
        'page arguments' => array(questionnaire_condition_create(array('type' => $type->type))),
        'access callback' => 'questionnaire_condition_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file' => 'questionnaire_condition.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Loading and editing questionnaire_condition entities
    $items[$this->path . '/questionnaire_condition/' . $wildcard] = array(
      'page callback' => 'questionnaire_condition_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'questionnaire_condition_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'questionnaire_condition.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/questionnaire_condition/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );
    
    $items[$this->path . '/questionnaire_condition/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'questionnaire_condition_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'questionnaire_condition_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'questionnaire_condition.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    
    // Menu item for viewing questionnaire_conditions
    $items['questionnaire_condition/' . $wildcard] = array(
      //'title' => 'Title',
      'title callback' => 'questionnaire_condition_page_title',
      'title arguments' => array(1),
      'page callback' => 'questionnaire_condition_page_view',
      'page arguments' => array(1),
      'access callback' => 'questionnaire_condition_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    return $items;
  }
  
  
  /**
   * Create the markup for the add QuestionnaireCondition Entities page within the class
   * so it can easily be extended/overriden.
   */ 
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }    
        
    return theme('questionnaire_condition_add_list', array('content' => $content));
  }
  
}


/**
 * Form callback wrapper: create or edit a questionnaire_condition.
 *
 * @param $questionnaire_condition
 *   The questionnaire_condition object being edited by this form.
 *
 * @see questionnaire_condition_edit_form()
 */
function questionnaire_condition_form_wrapper($questionnaire_condition) {
  // Add the breadcrumb for the form's location.
  questionnaire_condition_set_breadcrumb();
  return drupal_get_form('questionnaire_condition_edit_form', $questionnaire_condition);
}


/**
 * Form callback wrapper: delete a questionnaire_condition.
 *
 * @param $questionnaire_condition
 *   The questionnaire_condition object being edited by this form.
 *
 * @see questionnaire_condition_edit_form()
 */
function questionnaire_condition_delete_form_wrapper($questionnaire_condition) {
  // Add the breadcrumb for the form's location.
  //questionnaire_condition_set_breadcrumb();
  return drupal_get_form('questionnaire_condition_delete_form', $questionnaire_condition);
}


/**
 * Form callback: create or edit a questionnaire_condition.
 *
 * @param $questionnaire_condition
 *   The questionnaire_condition object to edit or for a create form an empty questionnaire_condition object
 *     with only a questionnaire_condition type defined.
 * 
 * might need to be removed.
 */
function questionnaire_condition_edit_form($form, &$form_state, $questionnaire_condition) {
  // Add the default field elements.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Condition Name'),
    '#default_value' => isset($questionnaire_condition->name) ? $questionnaire_condition->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );
  $form['plugin'] = array(
    '#type' => 'textfield',
    '#title' => t('Condition Plugin'),
    '#default_value' => isset($questionnaire_condition->operator) ? $questionnaire_condition->operator : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -4,
  );
  /**
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('QuestionnaireCondition Value'),
    '#default_value' => isset($questionnaire_condition->value) ? $questionnaire_condition->value : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -3,
  );*/
  
  $form['data']['#tree'] = TRUE;

  // Add the field related form elements.
  $form_state['questionnaire_condition'] = $questionnaire_condition;
  field_attach_form('questionnaire_condition', $questionnaire_condition, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save questionnaire_condition'),
    '#submit' => $submit + array('questionnaire_condition_edit_form_submit'),
  );
  
  if (!empty($questionnaire_condition->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete questionnaire_condition'),
      '#suffix' => l(t('Cancel'), 'admin/content/questionnaire_conditions'),
      '#submit' => $submit + array('questionnaire_condition_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'questionnaire_condition_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the questionnaire_condition form
 */
function questionnaire_condition_edit_form_validate(&$form, &$form_state) {
  $questionnaire_condition = $form_state['questionnaire_condition'];
  
  // Notify field widgets to validate their data.
  field_attach_form_validate('questionnaire_condition', $questionnaire_condition, $form, $form_state);
}


/**
 * Form API submit callback for the questionnaire_condition form.
 * 
 * @todo remove hard-coded link
 */
function questionnaire_condition_edit_form_submit(&$form, &$form_state) {
  
  $questionnaire_condition = entity_ui_controller('questionnaire_condition')->entityFormSubmitBuildEntity($form, $form_state);
  // Save the questionnaire_condition and go back to the list of questionnaire_conditions
  
  // Add in created and changed times.
  if ($questionnaire_condition->is_new = isset($questionnaire_condition->is_new) ? $questionnaire_condition->is_new : 0){
    $questionnaire_condition->created = time();
  }

  $questionnaire_condition->changed = time();
  
  $questionnaire_condition->save();
  $form_state['redirect'] = 'admin/content/questionnaire_conditions';
}

/**
 * Form API submit callback for the delete button.
 * 
 * @todo Remove hard-coded path
 */
function questionnaire_condition_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/questionnaire_conditions/questionnaire_condition/' . $form_state['questionnaire_condition']->questionnaire_condition_id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a questionnaire_condition.
 *
 * @param $questionnaire_condition
 *   The questionnaire_condition to delete
 *
 * @see confirm_form()
 */
function questionnaire_condition_delete_form($form, &$form_state, $questionnaire_condition) {
  $form_state['questionnaire_condition'] = $questionnaire_condition;

  $form['#submit'][] = 'questionnaire_condition_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete questionnaire_condition %name?', array('%name' => $questionnaire_condition->name)),
    'admin/content/questionnaire_conditions/questionnaire_condition',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  
  return $form;
}

/**
 * Submit callback for questionnaire_condition_delete_form
 */
function questionnaire_condition_delete_form_submit($form, &$form_state) {
  $questionnaire_condition = $form_state['questionnaire_condition'];

  questionnaire_condition_delete($questionnaire_condition);

  drupal_set_message(t('The questionnaire_condition %name has been deleted.', array('%name' => $questionnaire_condition->name)));
  watchdog('questionnaire_condition', 'Deleted questionnaire_condition %name.', array('%name' => $questionnaire_condition->name));

  $form_state['redirect'] = 'admin/content/questionnaire_conditions';
}



/**
 * Page to add QuestionnaireCondition Entities.
 *
 * @todo Pass this through a proper theme function
 */
function questionnaire_condition_add_page() {
  $controller = entity_ui_controller('questionnaire_condition');
  return $controller->addPage();
}


/**
 * Displays the list of available questionnaire_condition types for questionnaire_condition creation.
 *
 * @ingroup themeable
 */
function theme_questionnaire_condition_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="questionnaire_condition-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer questionnaire_condition types')) {
      $output = '<p>' . t('QuestionnaireCondition Entities cannot be added because you have not created any questionnaire_condition types yet. Go to the <a href="@create-questionnaire_condition-type">questionnaire_condition type creation page</a> to add a new questionnaire_condition type.', array('@create-questionnaire_condition-type' => url('admin/structure/questionnaire_condition_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No questionnaire_condition types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}





/**
 * Sets the breadcrumb for administrative questionnaire_condition pages.
 */
function questionnaire_condition_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('QuestionnaireConditions'), 'admin/content/questionnaire_conditions'),
  );

  drupal_set_breadcrumb($breadcrumb);
}



