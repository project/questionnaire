<?php

/**
 * This field handler aggregates operations that can be done on a questionnaire_condition
 * under a single field providing a more flexible way to present them in a view
 */
class questionnaire_condition_handler_questionnaire_condition_operations_field extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['questionnaire_condition_id'] = 'questionnaire_condition_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {

    $links = menu_contextual_links('questionnaire_condition', 'admin/content/questionnaire_conditions/questionnaire_condition', array($this->get_value($values, 'questionnaire_condition_id')));
    
    if (!empty($links)) {
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
