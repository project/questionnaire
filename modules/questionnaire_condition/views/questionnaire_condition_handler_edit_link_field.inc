<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying edit links
 * as fields
 */


class questionnaire_condition_handler_edit_link_field extends questionnaire_condition_handler_link_field {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }


  function render($values) {
    $type = $values->{$this->aliases['type']};
    
    //Creating a dummy questionnaire_condition to check access against
    $dummy_questionnaire_condition = (object) array('type' => $type);
    if (!questionnaire_condition_access('edit', $dummy_questionnaire_condition)) {
      return;
    }
    
    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $questionnaire_condition_id = $values->{$this->aliases['questionnaire_condition_id']};
    
    return l($text, 'admin/content/questionnaire_conditions/questionnaire_condition/' . $questionnaire_condition_id . '/edit');
  }
}
