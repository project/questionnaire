<?php

/**
 * @file
 * Providing extra functionality for the QuestionnaireCondition UI via views.
 */


/**
 * Implements hook_views_data()
 */
function questionnaire_condition_views_data_alter(&$data) { 
  $data['questionnaire_condition']['link_questionnaire_condition'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the questionnaire_condition.'),
      'handler' => 'questionnaire_condition_handler_link_field',
    ),
  );
  $data['questionnaire_condition']['edit_questionnaire_condition'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the questionnaire_condition.'),
      'handler' => 'questionnaire_condition_handler_edit_link_field',
    ),
  );
  $data['questionnaire_condition']['delete_questionnaire_condition'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the questionnaire_condition.'),
      'handler' => 'questionnaire_condition_handler_delete_link_field',
    ),
  );
  // This content of this field are decided based on the menu structure that
  // follows questionnaire_conditions/questionnaire_condition/%questionnaire_condition_id/op
  $data['questionnaire_condition']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this questionnaire_condition.'),
      'handler' => 'questionnaire_condition_handler_questionnaire_condition_operations_field',
    ),
  );
}


/**
 * Implements hook_views_default_views().
 */
function questionnaire_condition_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'questionnaire_conditions';
  $view->description = 'A list of all questionnaire_conditions';
  $view->tag = 'questionnaire_conditions';
  $view->base_table = 'questionnaire_condition';
  $view->human_name = 'QuestionnaireConditions';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'QuestionnaireConditions';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'create any questionnaire_condition type';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'questionnaire_condition_id' => 'questionnaire_condition_id',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'questionnaire_condition_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty ';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No questionnaire_conditions have been created yet';
  /* Field: QuestionnaireCondition: QuestionnaireCondition ID */
  $handler->display->display_options['fields']['questionnaire_condition_id']['id'] = 'questionnaire_condition_id';
  $handler->display->display_options['fields']['questionnaire_condition_id']['table'] = 'questionnaire_condition';
  $handler->display->display_options['fields']['questionnaire_condition_id']['field'] = 'questionnaire_condition_id';
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['questionnaire_condition_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['questionnaire_condition_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['questionnaire_condition_id']['empty_zero'] = 0;
  /* Field: QuestionnaireCondition: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'questionnaire_condition';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  /* Field: QuestionnaireCondition: Link */
  $handler->display->display_options['fields']['link_questionnaire_condition']['id'] = 'link_questionnaire_condition';
  $handler->display->display_options['fields']['link_questionnaire_condition']['table'] = 'questionnaire_condition';
  $handler->display->display_options['fields']['link_questionnaire_condition']['field'] = 'link_questionnaire_condition';
  $handler->display->display_options['fields']['link_questionnaire_condition']['label'] = 'View';
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['external'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['alter']['html'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['link_questionnaire_condition']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['link_questionnaire_condition']['hide_empty'] = 0;
  $handler->display->display_options['fields']['link_questionnaire_condition']['empty_zero'] = 0;
  /* Field: QuestionnaireCondition: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'questionnaire_condition';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['operations']['empty_zero'] = 0;
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'questionnaire_conditions_admin_page');
  $handler->display->display_options['path'] = 'admin/content/questionnaire_conditions/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'tab';
  $handler->display->display_options['tab_options']['title'] = 'QuestionnaireConditions';
  $handler->display->display_options['tab_options']['description'] = 'Manage questionnaire_conditions';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';
  $translatables['questionnaire_conditions'] = array(
    t('Master'),
    t('QuestionnaireConditions'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Empty '),
    t('No questionnaire_conditions have been created yet'),
    t('QuestionnaireCondition ID'),
    t('.'),
    t(','),
    t('Name'),
    t('View'),
    t('Operations links'),
    t('Page'),
  );
  $views[] = $view;
  return $views;

}
