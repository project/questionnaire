<?php
namespace Drupal\questionnaire_condition\Types;

/**
 * A class for defining a condition exportable.
 */
abstract class Condition implements ConditionInterface {
  protected $condition;
  protected $question;
  protected $visible;

  /**
   * Class constructor.
   *
   * @param $condition.
   *   The current instance of QuestionnaireCondition.
   * @param $question.
   *   A node of type question about which the condition applies.
   *
   * @return
   *   An instance of this plugin.
   */
  public function __construct($condition, $question) {
    $this->condition = $condition;
    $this->question = $question;

    // Adding a default condition to our states array, that states that
    // if the question about which this condition is about is not visible
    // then don't display this question.
    $this->visible = array(
      ':input[name="' . $this->condition->field_question['und'][0]['target_id'] . '"]' => array('filled' => TRUE),
      );
  }

  /**
   * Alters the $form passed in by reference according to the condition defined
   * in this plugin.
   *
   * @param $form.
   *   The form to be altered.
   */
  public function evaluate(&$form) {}
}
