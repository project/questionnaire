<?php
namespace Drupal\questionnaire_condition\Types;

/**
 * Defines an interface for plugins defining robot plugins.
 */
interface ConditionInterface {

  /**
   * Class constructor.
   *
   * @param $condition.
   *   The current instance of QuestionnaireCondition.
   * @param $question.
   *   A node of type question about which the condition applies.
   *
   * @return
   *   An instance of this plugin.
   */
  public function __construct($condition, $question);

  /**
   * Alters the $form passed in by reference according to the condition defined
   * in this plugin.
   *
   * @param $form.
   *   The form to be altered.
   */
  public function evaluate(&$form);
}
