<?php
/**
 * @file
 * Condition: IsNoneOf.
 *
 */

namespace Drupal\questionnaire_condition\Plugins\Condition;
use Drupal\questionnaire_condition\Types\Condition;

$plugin = array(
  'class' => '\\Drupal\\questionnaire_condition\\Plugins\\Condition\\IsNoneOf',
  'name' => 'Is None Of',
  'defaults' => array('datatypes' => array('Array')),
);

/**
 * Class IsNoneOf.
 *
 * @package Drupal\questionnaire_condition\Condition
 */
class IsNoneOf extends Condition {
  public function evaluate(&$form) {

    // First load the operand question and find out it's type.
    $oq = node_load($this->condition->field_question[LANGUAGE_NONE][0]['target_id']);
    $operand_question = entity_metadata_wrapper('node', $oq);
    $questiontype_plugin = ctools_get_plugins('questionnaire_question', 'QuestionType', $operand_question->questionnaire_question_type->value());

    // According to the answer datatype of the operand question, the visible array
    // changes
    switch ($questiontype_plugin['defaults']['answer_type']) {
      case 'Array':
        $visible_minis = array();
        foreach ($this->condition->value as $value) {
          $visible_minis[] = array(':input[name="' . $this->condition->field_question['und'][0]['target_id'] . '[' . $value . ']"]' => array('unchecked' => TRUE));
        }
        $visible = $visible_minis;
        break;
    }

    $this->visible = array_merge($this->visible, $visible);

    // Then add the visible array to the form elemets states array.
    if(empty($form['questions'][$this->question->nid->value()]['#states'])) {
      $form['questions'][$this->question->nid->value()]['#states']['visible'] = $this->visible;
    }
    else {
      $form['questions'][$this->question->nid->value()]['#states']['visible'] = array_merge($form['questions'][$this->question->nid->value()]['#states']['visible'], $this->visible);
    }
  }
}